from django.shortcuts import render

# Create your views here.


def landingPage(request):
    return render(request, 'landingPage.html')


def accordion(request):
    return render(request, 'accordion.html')
