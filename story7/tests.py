from django.test import TestCase, Client
from django.urls import reverse, resolve

# Create your tests here.


class Story7Test(TestCase):

    def setUp(self):
        self.client = Client()

    def test_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_landing_page_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_landing_page_content(self):
        response = Client().get('')
        self.assertIn("Stories", response.content.decode('utf8'))

    def test_accordion_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_accordion_url(self):
        response = Client().get('/accordion/')
        self.assertEquals(response.status_code, 200)

    def test_accordion_content(self):
        response = Client().get('/accordion/')
        self.assertIn("Achievements", response.content.decode('utf8'))


class BookshelvesTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_bookshelves_template(self):
        response = Client().get('/bookshelves/')
        self.assertTemplateUsed(response, 'bookshelves.html')

    def test_bookshelves_url(self):
        response = Client().get('/bookshelves/')
        self.assertEquals(response.status_code, 200)

    def test_bookshelves_content(self):
        response = Client().get('/bookshelves/')
        self.assertIn("Search", response.content.decode('utf8'))

    def test_views_ajax(self):
        response = Client().get(
            '/bookshelves/', {'q': 'Harry'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEquals(response.status_code, 200)
        self.assertTrue('fetched' in response.json())
